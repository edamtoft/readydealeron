const path = require("path")
const CopyWebpackPlugin = require("copy-webpack-plugin")

module.exports = {
  entry: "./index.js",
  mode: "production",
  output: {
    path: path.join(__dirname,"dist"),
    filename: 'bundle.min.js'
  },
  plugins: [
    new CopyWebpackPlugin(["index.html", "logo.png", "textures/*.png"])
  ]
};