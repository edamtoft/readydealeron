import voxelEngine from "voxel-engine";
import voxelPlayer from "voxel-player";
import highlight from "voxel-highlight";

const materials = [
  "10119",  "10125",  "10281",  "10319",  "10611",  "10612",  "10638",  "10640",  "10650",  "10826",  "11370",  "11482",  "11485",  "11512",  "11561",
  "11599",  "11630",  "11647",  "11921",  "12052",  "12119",  "12215",  "12323",  "12424",  "12427",  "12546",  "12618",  "12648",  "12768",  "12922",
  "13108",  "13423",  "13430",  "13657",  "13700",  "13737",  "14033",  "14074",  "14199",  "14202",  "14212",  "14282",  "14332",  "14375",  "14490",
  "14551",  "4861",  "4869",  "4973",  "4974",  "5004",  "5498",  "5520",  "6032",  "6205",  "6252",  "9814",  "9823",  "bmw",  "ford",  "honda",  "hyundai",
  "mazda",  "toyota",
];

const game = voxelEngine({
  texturePath: "textures/",
  materials,
  generate: function(x,y,z) {
    return y < 0 ? Math.round(Math.random() * materials.length) + 1 : 0;
  },
  chunkSize: 16,
  chunkDistance: 2,
});

game.appendTo(document.getElementById("__app__"));
window.game = game;


const playerFactory = voxelPlayer(game);
const player = playerFactory("textures/player-texture.png");

player.possess();
player.pov("first");
player.position.set(0, 0, 5);

const hl = highlight(game, { color: "#ff0000" });
let highlightPos;
hl.on("highlight", pos => highlightPos = pos);
hl.on("remove", pos => highlightPos = null);

game.on("fire", (target, state) => {
  if (highlightPos) game.setBlock(highlightPos, 0);
});

game.paused = false;

window.addEventListener("keydown", e => {
  if (e.keyCode === 79 || e.keyCode === 69) {
    const block = game.getBlock(highlightPos);
    if (block != 0) {
      const maybeDealerId = parseInt(materials[block-1]);
      if (!isNaN(maybeDealerId))
      {
        alert("Navigating to Dealer " + maybeDealerId);
        location.href = "https://dealer" + maybeDealerId + ".dealeron.com";
      }
    }
  }
});

window.addEventListener("keydown", e => {
  if (e.keyCode === 79 || e.keyCode === 69) {
    const block = game.getBlock(highlightPos);
    if (block != 0) {
      const maybeDealerId = parseInt(materials[block-1]);
      if (!isNaN(maybeDealerId))
      {
        alert("Navigating to Dealer " + maybeDealerId);
        location.href = "https://dealer" + maybeDealerId + ".dealeron.com";
      }
    }
  }
});